<?php

namespace Drupal\block_ip_stack;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ipstack\Ipstack;

/**
 * Ip Stack Service.
 */
class IpStackBlock {

  use StringTranslationTrait;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  /**
   * The IpStack service.
   *
   * @var \Drupal\ipstack\Ipstack
   */
  protected $ipStack;

  /**
   * The Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory object.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user object.
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $cache_tags_invalidator
   *   The cache tags invalidator service.
   * @param \Drupal\ipstack\Ipstack $ipstack
   *   The IpStack service.
   */
  public function __construct(
    ConfigFactory $config_factory,
    AccountProxyInterface $current_user,
    CacheTagsInvalidator $cache_tags_invalidator,
    Ipstack $ipstack
  ) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->ipStack = $ipstack;
  }

}
