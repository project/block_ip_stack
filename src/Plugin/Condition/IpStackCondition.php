<?php

namespace Drupal\block_ip_stack\Plugin\Condition;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ipstack\Ipstack;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CommerceGuys\Addressing\Country\CountryRepository;
use CommerceGuys\Addressing\Subdivision\SubdivisionRepository;

/**
 * Provides a 'IpStack' condition.
 *
 * @Condition(
 *   id = "ipstack",
 *   label = @Translation("Ipstack")
 * )
 */
class IpStackCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The IP Stack service.
   *
   * @var \Drupal\ipstack\Ipstack
   */
  protected $ipStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->setIpStack($container->get('ipstack'));
    return $instance;
  }

  /**
   * Setter for the ipstack service.
   *
   * @param \Drupal\ipstack\Ipstack $ipstack
   *   The IpStack service.
   */
  protected function setIpStack(Ipstack $ipstack) {
    $this->ipStack = $ipstack;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t('Ipstack');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'ipstack-form-wrapper';
    $form['#attached']['library'][] = 'block_ip_stack/block_ip_stack';

    $form['debug'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
      '#open' => TRUE,
    ];

    $form['debug']['debug_ip_address'] = [
      '#title' => $this->t('Debug IP Address'),
      '#type' => 'textfield',
      '#size' => 20,
      '#description' => $this->t('Enter in a IP address to test or debug conditions. This will override IP address found.'),
      '#default_value' => !empty($this->configuration['debug_ip_address']) ? $this->configuration['debug_ip_address'] : '',
    ];

    $form['conditions'] = [
      '#type' => 'details',
      '#title' => $this->t('Conditions'),
      '#open' => TRUE,
    ];

    $form['conditions']['notes'] = [
      '#markup' => '<p>' . $this->t('The criteria selected below will be used together as OR conditions.') . '</p>',
    ];

    $continents = [
      'AF' => $this->t('Africa'),
      'AN' => $this->t('Antarctica'),
      'AS' => $this->t('Asia'),
      'EU' => $this->t('Europe'),
      'NA' => $this->t('North America'),
      'OC' => $this->t('Oceania'),
      'SA' => $this->t('South America'),
    ];
    $form['conditions']['continents'] = [
      '#prefix' => '<div id="ajax-continents-wrapper">',
      '#suffix' => '</div>',
      '#title' => $this->t('Continents'),
      '#description' => $this->t('Select which continents can see this block.'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 10,
      '#options' => $continents,
      '#default_value' => !empty($this->configuration['continents']) ? $this->configuration['continents'] : [],
    ];

    $timezones = [];
    $currencies = [];

    $countryRepository = new CountryRepository();
    $countryList = $countryRepository->getAll();
    $countries = [];
    foreach ($countryList as $country) {
      $countries[$country->getCountryCode()] = $country->getName();
      foreach ($country->getTimezones() as $timezone) {
        $timezones[$timezone] = $timezone;
      }
      if (!empty($country->getCurrencyCode())) {
        $currencies[$country->getCurrencyCode()] = $country->getCurrencyCode();
      }
    }
    $default_countries = !empty($this->configuration['countries']) ? $this->configuration['countries'] : [];
    $form['conditions']['countries'] = [
      '#prefix' => '<div id="ajax-countries-wrapper">',
      '#suffix' => '</div>',
      '#title' => $this->t('Countries'),
      '#description' => $this->t('Select which countries can see this block.'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 10,
      '#options' => $countries,
      '#default_value' => $default_countries,
      '#ajax' => [
        'callback' => [$this, 'countriesAjaxCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Updating options...'),
        ],
      ],
    ];

    $subdivisionRepository = new SubdivisionRepository();
    $regions = [];
    foreach ($default_countries as $country_code) {
      $states = $subdivisionRepository->getAll([$country_code]);
      foreach ($states as $state) {
        $regions[$state->getCode()] = $state->getName();
      }
    }
    $form['conditions']['regions'] = [
      '#prefix' => '<div id="ajax-regions-wrapper">',
      '#suffix' => '</div>',
      '#title' => $this->t('Regions'),
      '#description' => $this->t('Select which regions can see this block.'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#validated' => TRUE,
      '#size' => 10,
      '#options' => $regions,
      '#default_value' => !empty($this->configuration['regions']) ? $this->configuration['regions'] : [],
    ];
    if (empty($regions)) {
      $form['conditions']['regions']['#prefix'] = '<div id="ajax-regions-wrapper" class="hidden">';
    }

    asort($timezones);
    $form['conditions']['timezones'] = [
      '#prefix' => '<div id="ajax-timezones-wrapper">',
      '#suffix' => '</div>',
      '#title' => $this->t('Timezones'),
      '#description' => $this->t('Select which timezones can see this block.'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#validated' => TRUE,
      '#size' => 10,
      '#options' => $timezones,
      '#default_value' => !empty($this->configuration['timezones']) ? $this->configuration['timezones'] : [],
    ];
    if (empty($regions)) {
      $form['conditions']['timezones']['#prefix'] = '<div id="ajax-timezones-wrapper" class="hidden">';
    }

    $form['conditions']['city'] = [
      '#title' => $this->t('City'),
      '#description' => $this->t('Enter in city name that can see this block.'),
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => !empty($this->configuration['city']) ? $this->configuration['city'] : '',
    ];

    $form['conditions']['zip'] = [
      '#title' => $this->t('Zip/Postal Code'),
      '#description' => $this->t('Enter in zip/postal code that can see this block.'),
      '#type' => 'textfield',
      '#size' => 12,
      '#default_value' => !empty($this->configuration['zip']) ? $this->configuration['zip'] : '',
    ];

    asort($currencies);
    $form['conditions']['currency'] = [
      '#prefix' => '<div id="ajax-currency-wrapper">',
      '#suffix' => '</div>',
      '#title' => $this->t('Currency'),
      '#description' => $this->t('Select currency that can see this block.'),
      '#type' => 'select',
      '#empty_option' => $this->t('- Select Currency -'),
      '#validated' => TRUE,
      '#options' => $currencies,
      '#default_value' => !empty($this->configuration['currency']) ? $this->configuration['currency'] : '',
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * Countries AJAX Callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The altered form.
   */
  public function countriesAjaxCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Initialize arrays.
    $currencies = [
      '' => $this->t('- Select Currency -'),
    ];
    $timezones = [];

    // Get countries selected.
    $values = $form_state->getValues();
    if (!empty($values['visibility']['ipstack']['conditions']['countries'])) {
      $countries = array_keys($values['visibility']['ipstack']['conditions']['countries']);
    }
    else {
      $countries = [];
    }

    // Update regions.
    $subdivisionRepository = new SubdivisionRepository();
    if (!empty($countries)) {
      $regions = [];
      foreach ($countries as $country_code) {
        $states = $subdivisionRepository->getAll([$country_code]);
        foreach ($states as $state) {
          $regions[$state->getCode()] = $state->getName();
        }
      }

      if (empty($regions)) {
        $form['visibility']['ipstack']['conditions']['regions']['#prefix'] = '<div id="ajax-regions-wrapper" class="hidden">';
        $form['visibility']['ipstack']['conditions']['timezones']['#prefix'] = '<div id="ajax-timezones-wrapper" class="hidden">';
      }
      else {
        $form['visibility']['ipstack']['conditions']['regions']['#prefix'] = '<div id="ajax-regions-wrapper">';
        $form['visibility']['ipstack']['conditions']['timezones']['#prefix'] = '<div id="ajax-timezones-wrapper">';
      }

      $form['visibility']['ipstack']['conditions']['regions']['#options'] = $regions;
    }

    // Update timezones.
    $countryRepository = new CountryRepository();
    $countryList = $countryRepository->getAll();
    foreach ($countryList as $country) {
      if (in_array($country->getCountryCode(), $countries)) {
        foreach ($country->getTimezones() as $timezone) {
          $timezones[$timezone] = $timezone;
        }
        if (!empty($country->getCurrencyCode())) {
          $currencies[$country->getCurrencyCode()] = $country->getCurrencyCode();
        }
      }
    }
    $form['visibility']['ipstack']['conditions']['timezones']['#options'] = $timezones;

    // Update currencies.
    $form['visibility']['ipstack']['conditions']['currency']['#options'] = $currencies;

    // Update form fields.
    $response->addCommand(new ReplaceCommand('#ajax-regions-wrapper', $form['visibility']['ipstack']['conditions']['regions']));
    $response->addCommand(new ReplaceCommand('#ajax-timezones-wrapper', $form['visibility']['ipstack']['conditions']['timezones']));
    $response->addCommand(new ReplaceCommand('#ajax-currency-wrapper', $form['visibility']['ipstack']['conditions']['currency']));

    // Return AJAX response.
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $debug = $values['debug'];
    $conditions = $values['conditions'];

    $this->configuration['debug_ip_address'] = $debug['debug_ip_address'];
    $this->configuration['continents'] = $conditions['continents'];
    $this->configuration['countries'] = array_keys($conditions['countries']);
    $this->configuration['regions'] = $conditions['regions'];
    $this->configuration['timezones'] = $conditions['timezones'];
    $this->configuration['city'] = $conditions['city'];
    $this->configuration['zip'] = $conditions['zip'];
    $this->configuration['currency'] = $conditions['currency'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $status = FALSE;

    if (!empty($this->configuration['debug_ip_address'])) {
      $ip = $this->configuration['debug_ip_address'];
    }
    else {
      $ip = \Drupal::request()->getClientIp();
    }

    $this->ipStack->setIp($ip);
    $response = $this->ipStack->getData();

    if (!empty($response['data']) && is_string($response['data'])) {
      $data = json_decode($response['data']);
    }
    else {
      return FALSE;
    }

    // Check continent code.
    if (!empty($data->continent_code) && in_array($data->continent_code, $this->configuration['continents'])) {
      $status = TRUE;
    }

    // Check country code.
    if (!empty($data->country_code) && in_array($data->country_code, $this->configuration['countries'])) {
      $status = TRUE;
    }

    // Check region code.
    if (!empty($data->region_code) && in_array($data->region_code, $this->configuration['regions'])) {
      $status = TRUE;
    }

    // Check timezone.
    if (!empty($data->time_zone->id) && in_array($data->time_zone->id, $this->configuration['timezones'])) {
      $status = TRUE;
    }

    // Check city.
    if (!empty($data->city) && strtolower($data->city) === strtolower($this->configuration['city'])) {
      $status = TRUE;
    }

    // Check zip.
    if (!empty($data->zip) && $data->zip === $this->configuration['zip']) {
      $status = TRUE;
    }

    if (parent::isNegated()) {
      return !$status;
    }
    else {
      return $status;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'debug_ip_address' => '',
      'continents' => [],
      'countries' => [],
      'regions' => [],
      'timezones' => [],
      'city' => '',
      'zip' => '',
      'currency' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    $cache_contexts = [
      'ip',
    ];
    return Cache::mergeContexts($cache_contexts, parent::getCacheContexts());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $tags = [
      'ipstack',
    ];
    return Cache::mergeTags($tags, parent::getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    $max_age = 0;
    return Cache::mergeMaxAges(parent::getCacheMaxAge(), $max_age);
  }

}
